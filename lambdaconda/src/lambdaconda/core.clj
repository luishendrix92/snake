(ns lambdaconda.core
  (:require [lanterna.terminal :as t]))

(def actions {:escape #(assoc %1 :running false)
              :up     #(assoc %1 :new-direction [0 -1])
              :right  #(assoc %1 :new-direction [1  0])
              :down   #(assoc %1 :new-direction [0  1])
              :left   #(assoc %1 :new-direction [-1 0])})
(def term (t/get-terminal :swing))

(defn process-keystrokes [state]
  (->> (repeatedly #(t/get-key term))
       (take-while some?)
       (map #(get actions %1 identity))
       (reduce #(%2 %1) state)))

(defn gameloop [state]
  (let [new-state (process-keystrokes state)]
    (when (:running new-state)
      (do (Thread/sleep 1000)
          (recur new-state)))))

(defn -main [& args]
  (do (t/start term)
      (gameloop {:running       true
                 :direction     [0 0]
                 :new-direction nil
                 :size          [80 24]
                 :body          [[37 12] [38 12] [39 12]]
                 :food-pos      [25 18]})
      (t/stop term)))

