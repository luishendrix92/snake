import Html exposing (..)
import Svg exposing (svg, rect)
import Svg.Attributes as SA
import Maybe exposing (withDefault)
import Tuple exposing (..)
import Time exposing (millisecond, every)
import Keyboard exposing (KeyCode)
import Mechanics exposing (gameLoop, preventOpposite)
import Helpers exposing (..)
import Types exposing (..)

initState =
  { size = (30, 20)
  , head = (5, 5)
  , tail = []
  , direction = (0, 0)
  , foodPos = Nothing }

update : Msg -> State -> (State, Cmd Msg)
update msg ({ direction } as state) =
  case msg of
    Tick _ -> gameLoop state initState
    Spawn newFood -> ({ state | foodPos = Just newFood }, Cmd.none)
    KeyMsg code ->
      let
        newDirection = code |> toDirection direction |> preventOpposite direction
      in
        ({ state | direction = newDirection }, Cmd.none)

subscriptions : State -> Sub Msg
subscriptions _ =
  Sub.batch
    [ Keyboard.downs KeyMsg
    , every (millisecond * 200) Tick ]

block : String -> Vector -> Html Msg
block class (x, y) =
  rect [ SA.x (toString x), SA.y (toString y)
       , SA.class ("block " ++ class) ] []

view : State -> Html Msg
view ({ head, tail, foodPos, size }) =
  let
    (width, height) = mapTuple toString size
    snakeBlocks = (block "snake head" head) :: (List.map (block "snake tail") tail)
  in
    svg [ SA.viewBox ("0 0 " ++ width ++ " " ++ height), SA.class "field" ]
      (foodPos
        |> Maybe.map (block "food" >> consTo snakeBlocks)
        |> withDefault snakeBlocks)

main =
  Html.program
    { init = (initState, Cmd.none)
    , view = view
    , subscriptions = subscriptions
    , update = update }