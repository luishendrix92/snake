module Helpers exposing (..)

import Types exposing (Vector)

-- Tuple Helpers

mapTuple : (a -> b) -> (a, a) -> (b, b)
mapTuple f (a, b) = (f a, f b)

addVectors : Vector -> Vector -> Vector
addVectors (a, b) (c, d) = (a + c, b + d)

flipVector : Vector -> Vector
flipVector = mapTuple negate

-- List Helpers

consTo : List a -> a -> List a
consTo = flip (::)

butLast : List a -> List a
butLast xs =
  case xs of
    []        -> []
    [x]       -> []
    (x :: xs) -> x :: butLast xs

-- App Helpers

toDirection : Vector -> Int -> Vector
toDirection currentDirection charCode =
  case charCode of
    37 -> (-1, 0)
    38 -> (0, -1)
    39 -> (1,  0)
    40 -> (0,  1)
    _  -> currentDirection