import Html exposing (..)
import Svg exposing (svg, rect)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)
import Svg.Attributes as SA
import Maybe exposing (withDefault)
import Tuple exposing (..)
import Time exposing (millisecond, every)
import Keyboard exposing (KeyCode)
import Mechanics exposing (gameLoop, preventOpposite)
import Helpers exposing (..)
import Types exposing (..)

initState =
  { fps = 5
  , score = 0
  , size = (30, 20)
  , head = (5, 5)
  , tail = []
  , direction = (0, 0)
  , foodPos = Nothing }

update : Msg -> State -> (State, Cmd Msg)
update msg ({ direction, fps, size } as state) =
  case msg of
    Tick _ -> gameLoop state initState
    Spawn newFood -> ({ state | foodPos = Just newFood }, Cmd.none)
    ChangeFps newFpsStr ->
      ({ state | fps = parseFloat 5 newFpsStr }, Cmd.none)
    ChangeWidth newWidth ->
      ({ state | size = (parseInt 30 newWidth, second size) }, Cmd.none)
    ChangeHeight newHeight ->
      ({ state | size = (first size, parseInt 20 newHeight) }, Cmd.none)
    KeyMsg code ->
      let
        newDirection = code |> toDirection direction |> preventOpposite direction
      in
        ({ state | direction = newDirection }, Cmd.none)

subscriptions : State -> Sub Msg
subscriptions ({ fps }) =
  Sub.batch
    [ Keyboard.downs KeyMsg
    , every (millisecond * 1000 / fps) Tick ]

block : String -> Vector -> Html Msg
block class (x, y) =
  rect [ SA.x (toString x), SA.y (toString y)
       , SA.class ("block " ++ class) ] []

field : Int -> State -> Html Msg
field blockSize ({ head, tail, foodPos, size }) =
  let
    (width, height) = mapTuple toString size
    snakeBlocks = (block "snake head" head) :: (List.map (block "snake tail") tail)
  in
    svg [ SA.viewBox ("0 0 " ++ width ++ " " ++ height), SA.class "field" ]
      (foodPos
        |> Maybe.map (block "food" >> consTo snakeBlocks)
        |> withDefault snakeBlocks)

settings : Float -> Vector -> Html Msg
settings fps (width, height) =
  div [ class "row" ]
    [ div [ class "column" ]
        [ label [ for "fps" ] [ text "Snake speed (FPS)" ]
        , input [ type_ "number", value (toString fps)
                , onInput ChangeFps, id "fps" ] [] ]
    , div [ class "column" ]
        [ label [ for "width" ] [ text "Width" ]
        , input [ type_ "number", value (toString width)
                , onInput ChangeWidth, id "width" ] [] ]
    , div [ class "column" ]
        [ label [ for "height" ] [ text "Height" ]
        , input [ type_ "number", value (toString height)
                , onInput ChangeHeight, id "height" ] [] ] ]

view : State -> Html Msg
view ({ fps, size, score } as state) =
  div []
    [ settings fps size
    , field 20 state
    , p [ class "score" ]
        [ b [] [ text "Score: " ]
        , text (toString score) ] ]

main =
  Html.program
    { init = (initState, Cmd.none)
    , view = view
    , subscriptions = subscriptions
    , update = update }