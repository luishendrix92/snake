module Mechanics exposing (gameLoop, preventOpposite)

import Types exposing (..)
import Guards exposing (..)
import Tuple exposing (first, second)
import Random exposing (..)
import Helpers exposing (..)
import Maybe exposing (withDefault)

preventOpposite : Vector -> Vector -> Vector
preventOpposite prevDir newDir
  = flipVector newDir == prevDir => prevDir
 |= newDir

moveHead : State -> State
moveHead ({ head, tail, direction } as state) =
  { state | head = addVectors head direction, tail = head :: tail }

eatFood : State -> State
eatFood ({ head, tail, foodPos, score } as state)
  = (foodPos == Just head) => { state | foodPos = Nothing, score = score + 1 }
 |= { state | tail = butLast tail }

collision : State -> Maybe State
collision ({ head, tail, size } as state)
  = List.any ((==) head) tail => Nothing
 |= (first head >= first size || second head >= second size) => Nothing
 |= (first head < 0 || second head < 0) => Nothing
 |= Just state

spawnApple : Vector -> Cmd Msg
spawnApple (width, height) =
  let
    randomPos = pair (int 0 (width - 1)) (int 0 (height - 1))
  in
    generate Spawn randomPos

newFoodCmd : State -> (State, Cmd Msg)
newFoodCmd ({ foodPos, size } as state) =
  case foodPos of
    Nothing -> (state, spawnApple size)
    Just _  -> (state, Cmd.none)

gameLoop : State -> State -> (State, Cmd Msg)
gameLoop ({ size } as state) initState =
  state
    |> moveHead
    |> eatFood
    |> collision
    |> Maybe.map newFoodCmd
    |> withDefault (initState, spawnApple size)